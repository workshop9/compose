# install `pdo_mysql`

```
docker exec -ti realworld-compose_api_1 docker-php-ext-install pdo_mysql
```

# migrate

```
docker exec -ti realworld-compose_api_1 php artisan migrate
```

# seed

```
docker exec -ti realworld-compose_api_1 php artisan migrate:refresh
docker exec -ti realworld-compose_api_1 php artisan db:seed
```

# fix seeds

```
composer update fzaninotto/faker
```

# [Portainer in windows 10 setup](https://lemariva.com/blog/2018/05/tutorial-portainer-for-local-docker-environments-on-windows-10)
